import argparse

weektext = """
## Monday

## Tuesday

## Wednesday

## Thursday

## Friday

"""

def draw_timing_diagram(fn):
    with open(fn,'a') as file:
        file.write(weektext)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('fn',help='the name of the file to ouput ')
    parser.add_argument('week',help='print week date')
    args = parser.parse_args()
    print({args.fn})
    print("hello world!\n")
    draw_timing_diagram(args.fn)
    
if __name__ == '__main__':
    main()