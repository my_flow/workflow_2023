[TOC]

2023/11/15
1. 可以更新gen_stub脚本为project模式，生成stub更快。或者更新脚本（GateLvl）。
2. 添加vu440到脚本flow里面,参照fpga_vivado.pl脚本，或者熟悉使用fpga_vivado.pl脚本。
3. 脚本里面应该去掉monitor相应的文件与宏定义

2023/11/27
1.写一个python脚本，生成struct结构图
2.模块化fpga的各个文件，比如block design。然后可以通过脚本来组装调用。
